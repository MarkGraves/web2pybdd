#!/usr/bin/env python

from datetime import datetime, time, timedelta

from Line import Line

class TimetableService():
    def __init__(self):
        self.buildLines()
        self.buildDepartureTimes()


    def buildLines(self):
        lines = []
        lines.append(Line.named("Western").departingFrom("Emu Plains").withStations(["Emu Plains", "Parramatta", "Town Hall", "North Richmond"]))
        lines.append(Line.named("Western").departingFrom("North Richmond").withStations(["North Richmond", "Town Hall", "Parramatta", "Emu Plains"]))
        lines.append(Line.named("Epping").departingFrom("Epping").withStations(["Epping", "Strathfield", "Central"]))
        lines.append(Line.named("Epping").departingFrom("City").withStations(["Central", "Strathfield", "Epping"]))
        self.lines = lines

    def buildDepartureTimes(self):
        times = []
        times.append(time(7,53))
        times.append(time(7,55))
        times.append(time(7,57))
        times.append(time(8,6))
        times.append(time(8,9))
        times.append(time(8,16))
        self.universalDepartureTimes = times

    def findArrivalTimes(self, line, targetStation):
        """ generated source for method findArrivalTimes """
        targetLine = self.lineMatching(line)

        timeTaken = 0

        for station in targetLine.getStations():
            if station == targetStation:
                break
            timeTaken += 5

        arrivalTimes = []

        now = datetime.now()

        for _time in self.universalDepartureTimes:
            combined = datetime.combine(datetime.today(), _time)
            combined += timedelta(seconds=(timeTaken*60))
            arrivalTimes.append(combined.time())

        return arrivalTimes

    def lineMatching(self, requestedLine):
        """ generated source for method lineMatching """
        for line in self.lines:
            if line.equals(requestedLine):
                return line
        return None

    def findLinesThrough(self, departure, destination):
        """ generated source for method findLinesThrough """
        resultLines = []
        for line in self.lines:
            stations = line.getStations()
            if ((departure in stations) and (destination in stations)):
                if stations.index(destination) > stations.index(departure):
                    resultLines.append(line)
        return resultLines

    def scheduleArrivalTime(self, line, departureTime):
        """ generated source for method scheduleArrivalTime """

    def getArrivalTime(self, travellingOnLine, destination):
        """ generated source for method getArrivalTime """
        return None