#!/usr/bin/env python
""" generated source for module Line """
class LineBuilder(object):

    def __init__(self, lineName):
        """ generated source for method __init__ """
        self.lineName = lineName

    def departingFrom(self, station):
        """ generated source for method departingFrom """
        return Line(self.lineName, station)

class Line(object):
    """ generated source for class Line """

    def __init__(self,lineName,station):
        """ generated source for method __init__ """
        self.lineName = lineName
        self.station = station

    def departingFrom(self):
        """ generated source for method getDepartingFrom """
        return self.station

    def getLine(self):
        """ generated source for method getLine """
        return self.line

    def getStations(self):
        """ generated source for method getStations """
        return self.stations

    def withStations(self, stations):
        self.stations = stations
        return self

    @classmethod
    def named(cls, lineName):
        """ generated source for method named """
        return LineBuilder(lineName)

    def equals(self, o):
        """ generated source for method equals """
        if self == o:
            return True
        if o == None or self.__class__ != o.__class__:
            return False
        line1 = o
        if self.departingFrom == None:
            return False

        if line1.departingFrom == None:
            return False
        if self == None:
            return False
        if line1 == None:
            return False
        if self.departingFrom() == line1.departingFrom():
            return True
        return False

    def hashCode(self):
        """ generated source for method hashCode """
        result = self.line.hashCode() if self.line != None else 0
        result = 31 * result + (departingFrom.hashCode() if departingFrom != None else 0)
        return result