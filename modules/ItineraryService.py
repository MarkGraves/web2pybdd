#!/usr/bin/env python
""" generated source for module ItineraryService """
from TimetableService import TimetableService
from Line import Line

class ItineraryService(object):
    """ generated source for class ItineraryService """

    def __init__(self, timetableService):
        """ generated source for method __init__ """
        self.timetableService = TimetableService()
        self.MAX_ARRIVAL_TIMES = 3

    def findNextDepartures(self, departure, destination, startTime):
        """ generated source for method findNextDepartures """

        lines = self.timetableService.findLinesThrough(departure, destination)

        allArrivalTimes = self.getArrivalTimesOnLines(lines, departure)

        candidateArrivalTimes = self.findArrivalTimesAfter(startTime, allArrivalTimes)

        return self.atMost(self.MAX_ARRIVAL_TIMES, candidateArrivalTimes)

    def getArrivalTimesOnLines(self, lines, station):
        """ generated source for method getArrivalTimesOnLines """
        allArrivalTimes = []

        for line in lines:
            times = self.timetableService.findArrivalTimes(line, station)
            allArrivalTimes.extend(times)

        return allArrivalTimes

    def atMost(self, maxTimes, candidateTimes):
        """ generated source for method atMost """
        return candidateTimes[0:maxTimes]

    def findArrivalTimesAfter(self, startTime, allArrivalTimes):
        """ generated source for method findArrivalTimesAfter """
        viableArrivalTimes = []
        for arrivalTime in allArrivalTimes:
            if arrivalTime > startTime:
                viableArrivalTimes.append(arrivalTime)
        return viableArrivalTimes
