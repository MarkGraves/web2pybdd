"""Provide travelers with optimal itineraries feature tests."""
import pytest
import time
import re

from pytest_bdd import given, scenario, then, when

from datetime import time


@scenario('../features/provide_travelers_with_optimal_itineraries.feature', 'Find the optimal itinerary between stations on the same line')
def test_find_the_optimal_itinerary_between_stations_on_the_same_line():
    pass

@given(re.compile('(?P<line_name>\w+) line trains from (?P<departing_from>\w+ \w+) leave Parramatta to Town Hall at 7:58, 8:00, 8:02, 8:11, 8:14, 8:21'),
       converters=dict(line_name=str,
                       departing_from=str))
def i_want_to_travel(xline,line_name,departing_from):
    return dict(line=xline.Line.named(line_name).departingFrom(departing_from))


@when(re.compile('I want to travel from (?P<departure>\w+) to (?P<destination>\w+ \w+) at (?P<start_time>\S+)'),
      converters=dict(departure=str,
                      desination=str,
                      start_time=str))
def when_i_want_to_travel_on(i_want_to_travel,departure,destination,start_time):
    i_want_to_travel['departure'] = departure
    i_want_to_travel['destination'] = destination
    hours = int(start_time.split(':')[0])
    mins = int(start_time.split(':')[1])
    i_want_to_travel['startTime'] =time(hours,mins)
    pass

@then('I should be told about the trains at 8:02, 8:11, 8:14')
def should_find_these_trains(i_want_to_travel,its,tt):

    expected_departure_times = [time(8,2), time(8,11),time(8,14)]

    timetable = tt.TimetableService()
    itinerary = its.ItineraryService(timetable)

    departure = i_want_to_travel['departure']
    destination = i_want_to_travel['destination']
    startTime = i_want_to_travel['startTime']

    proposed_departure_times = itinerary.findNextDepartures(departure, destination, startTime)

    assert(proposed_departure_times == expected_departure_times)


