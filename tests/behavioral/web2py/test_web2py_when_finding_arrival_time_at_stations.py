
import pytest
import time
import re

from pytest_bdd import given, scenario, then, when

from datetime import time

@scenario('../../features/find_arrival_times_at_station.feature', 'Find arrival times for a station on a line')
def test_find_arrival_times_at_station():
    pass

@given(re.compile('I am traveling on the (?P<line_name>\w+) line from (?P<departing_from>\w+ \w+)'),
       converters=dict(line_name=str,departing_from=str))
def i_want_to_travel(line_name,departing_from,web2py):
    web2py.request.vars.lineName = line_name
    web2py.request.vars.departingFrom = departing_from

    return dict(line_name=line_name,
                departing_from=departing_from)


@when(re.compile('I want to travel to (?P<destination>\w+)'),
      converters=dict(destination=str))
def when_i_want_to_travel_to(destination,web2py):
    web2py.request.vars.destination = destination
    pass

@then('I should be told about the trains at 7:58, 8:00, 8:02, 8:11, 8:14, 8:21')
def should_find_these_times(web2py):
    expected_arrival_times = [time(7,58), time(8,0), time(8,2),
                              time(8,11),time(8,14), time(8,21)]

    proposed_arrival_times = web2py.run('timetable', 'calculate_arrival_times_at_stations', web2py)

    assert(proposed_arrival_times == expected_arrival_times)


