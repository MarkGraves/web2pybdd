"""Provide travelers with optimal itineraries feature tests."""
import pytest
from datetime import time



from pytest_bdd import given, scenario, then, when

@scenario('../../features/calculate_arrival_times.feature','Calculate arrival times',
         example_converters=dict(departure=str, destination=str,
                                 departure_time=str, line=str,
                                 arrival_time=str))
def test_calculate_arrival_times():
    """Calculate arrival times."""
    pass


@given('I want to go from <departure> to <destination>')
def i_want_to_go(departure,destination):
    return dict(_from=departure,_to=destination)

@given('the next train leaves at <departure_time> on <line> line')
def the_next_train_leaves_at(i_want_to_go, departure_time, line):
    """the next train leaves at <departure_time> on <line> line."""
    i_want_to_go['at_time'] = departure_time
    i_want_to_go['on_line'] = line
    return i_want_to_go


@when('I ask for my arrival time')
def i_ask_for_my_arrival_time(i_want_to_go,web2py):
    # Looks like a behavioral/functional test rather than unit because it uses two classes

    lineName = i_want_to_go['on_line']

    departure = i_want_to_go['_from']
    destination = i_want_to_go['_to']
    startTime = i_want_to_go['at_time']

    web2py.request.vars.lineName = lineName
    web2py.request.vars.departure = departure
    web2py.request.vars.destination = destination
    web2py.request.vars.startTime = startTime

    result = web2py.run('timetable', 'calculate_arrival_times', web2py)

    i_want_to_go['proposed'] = result
    return i_want_to_go

@then('the estimated arrival time should be <arrival_time>')
def the_estimated_arrival_time_should_be_arrivaltime(i_want_to_go, arrival_time):
    """the estimated arrival time should be <arrival-time>."""
    expected_arrival_time = time(int(arrival_time[0]),int(arrival_time[2:4]))

    proposed_arrival_time = i_want_to_go['proposed']
    assert(proposed_arrival_time == expected_arrival_time)
