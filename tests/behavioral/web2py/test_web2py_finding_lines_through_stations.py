import pytest
import time

from pytest_bdd import given, scenario, then, when
from datetime import time

@scenario('../../features/find_lines_through_stations.feature',
          'Find lines running through stations',
          example_converters=dict(lineName=str, lineDeparture=str,
                                 departure=str, destination=str))
def test_find_lines_through_stations():
    pass

@given('I am traveling on the <lineName> line from <lineDeparture>')
def given_line_named_departing_from(lineName,lineDeparture,xline):
    return dict()


@when('I want to travel from <departure> to <destination>')
def when_i_want_to_travel_to(given_line_named_departing_from, departure,destination):
    given_line_named_departing_from['departure'] = departure
    given_line_named_departing_from['destination'] = destination

@then('I should leave <lineName> from <lineDeparture>')
def should_leave_line_from_origin(given_line_named_departing_from, lineName,lineDeparture,web2py):
    expected_line = lineName
    expected_departure = lineDeparture

    departure = given_line_named_departing_from['departure']
    destination = given_line_named_departing_from['destination']

    web2py.request.vars.travelerDeparture = departure
    web2py.request.vars.travelerDestination = destination

    result = web2py.run('timetable', 'find_lines_through_stations', web2py)

    assert(result['lineName'] == expected_line)
    assert(result['departure'] == expected_departure)
