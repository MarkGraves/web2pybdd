
import pytest
import time
import re

from pytest_bdd import given, scenario, then, when

from datetime import time

@scenario('../features/find_arrival_times_at_station.feature', 'Find arrival times for a station on a line')
def test_find_arrival_times_at_station():
    pass

@given(re.compile('I am traveling on the (?P<line_name>\w+) line from (?P<departing_from>\w+ \w+)'),
       converters=dict(line_name=str,departing_from=str))
def given_line_named_departing_from(xline,line_name,departing_from):
    return dict(line=xline.Line.named(line_name).departingFrom(departing_from))


@when(re.compile('I want to travel to (?P<destination>\w+)'),
      converters=dict(destination=str))
def when_i_want_to_travel_to(given_line_named_departing_from,destination):
    given_line_named_departing_from['destination'] = "Parramatta"
    pass

@then('I should be told about the trains at 7:58, 8:00, 8:02, 8:11, 8:14, 8:21')
def should_find_these_times(given_line_named_departing_from,its,tt):

    expected_arrival_times = [time(7,58), time(8,0), time(8,2),
                              time(8,11),time(8,14), time(8,21)]

    destination = given_line_named_departing_from['destination']
    line = given_line_named_departing_from['line']

    service = tt.TimetableService()

    proposed_arrival_times = service.findArrivalTimes(line, destination)

    assert(proposed_arrival_times == expected_arrival_times)


