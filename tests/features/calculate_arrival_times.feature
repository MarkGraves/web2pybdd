Feature: Calculate arrival times between two stations on a single line

Scenario Outline: Calculate arrival times
    Given I want to go from <departure> to <destination>
    And the next train leaves at <departure_time> on <line> line
    When I ask for my arrival time
    Then the estimated arrival time should be <arrival_time>

    Examples:
    | line      | departure      | destination | departure_time | arrival_time |
    | Western   | Emu Plains     | Town Hall   | 8:02           | 8:03         |
    | Western   | North Richmond | Parramatta  | 8:03           | 8:05         |
    | Newcastle | Epping         | Central     | 8:07           | 8:16         |
    | Epping    | Epping         | Central     | 8:13           | 8:16         |
