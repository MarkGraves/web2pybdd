Feature: Provide travelers with optimal itineraries

Scenario: Find the optimal itinerary between stations on the same line
    Given Western line trains from Emu Plains leave Parramatta to Town Hall at 7:58, 8:00, 8:02, 8:11, 8:14, 8:21
    When I want to travel from Parramatta to Town Hall at 8:00
    Then I should be told about the trains at 8:02, 8:11, 8:14
