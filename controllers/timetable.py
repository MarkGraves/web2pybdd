from datetime import time

from applications.bdd.modules import TimetableService
from applications.bdd.modules import ItineraryService
from applications.bdd.modules import Line


def find_lines_through_stations():
    travelerDeparture = request.vars.travelerDeparture
    travelerDestination = request.vars.travelerDestination

    service = TimetableService.TimetableService()
    proposed_line = service.findLinesThrough(travelerDeparture,
                                             travelerDestination)[0]

    proposed = {}
    proposed['lineName'] = proposed_line.lineName
    proposed['departure'] = proposed_line.departingFrom()

    return proposed

def calculate_arrival_times():
    lineName = request.vars.lineName
    departure = request.vars.departure
    destination = request.vars.destination
    startTime = request.vars.startTime

    timetable = TimetableService.TimetableService()

    line = Line.Line.named(lineName).departingFrom(departure)

    allArrivalTimes = timetable.findArrivalTimes(line, destination)

    hours = int(startTime[0])
    minutes = int(startTime[2:])

    _startTime = time(hours,minutes)

    service = ItineraryService.ItineraryService(timetable)
    proposed_arrival_time = service.findArrivalTimesAfter(_startTime, allArrivalTimes)[0]

    return proposed_arrival_time

def calculate_arrival_times_at_stations():
    lineName = request.vars.lineName
    departure = request.vars.departingFrom
    destination = request.vars.destination
    line = Line.Line.named(lineName).departingFrom(departure)

    service = TimetableService.TimetableService()

    proposed_arrival_times = service.findArrivalTimes(line, destination)

    return proposed_arrival_times

def find_optimal_itineraries():
    departure = request.vars.departure
    destination = request.vars.destination
    startTime = request.vars.startTime


    timetable = TimetableService.TimetableService()
    itinerary = ItineraryService.ItineraryService(timetable)

    hours = int(startTime.split(':')[0])
    mins = int(startTime.split(':')[1])
    _startTime = time(hours,mins)

    proposed_times = itinerary.findNextDepartures(departure,
                                                  destination,
                                                  _startTime)
    return proposed_times



